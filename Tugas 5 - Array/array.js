
console.log('no 1') 
function range(startNum='', finishNum='') {
    if (startNum==''||finishNum==''){
        return -1;
    } else {
        var angka = [];
        if (startNum < finishNum){
            for(var x = startNum; x <= finishNum; x++) {
                angka.push(x);
            }
        } else {
            for(var x = startNum; x >= finishNum; x--) {
                angka.push(x);
            }
        }
        return angka;
    }
}
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log(' ') 
console.log('no 2') 
function rangeWithStep(startNum='', finishNum='', step='') {
    
    var angka = [];
    if (startNum < finishNum){
        var x = startNum;
        while(x <= finishNum) {
            angka.push(x);
            x = x + step;
        }
    } else {
        var x = startNum;
        while(x >= finishNum) {
            angka.push(x);
            x = x - step;
        }
    }
    return angka;

}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log(' ') 
console.log('no 3') 
function sum(startNum='', finishNum='', step=1) {
    
    var angka = [];
    if (startNum < finishNum){
        var x = startNum;
        while(x <= finishNum) {
            angka.push(x);
            x = x + step;
        }
    } else {
        var x = startNum;
        while(x >= finishNum) {
            angka.push(x);
            x = x - step;
        }
    }
    var total = 0;
    for (i=0;i < angka.length;i++){
        total = total + angka[i];
    }
    
    return total;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log(' ') 
console.log('no 4') 
function dataHandling(input) {
    for (x=0;x<input.length;x++){
        console.log("Nomor ID: "  + input[x][0]);
        console.log("Nama Lengkap: "  + input[x][1]);
        console.log("TTL: "  + input[x][2] + " " + input[x][3]);
        console.log("Hobi: "  + input[x][4] + "\n");
    }
        
}

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input);


console.log(' ') 
console.log('no 5') 

function balikKata(input) {
    var balik='';
    for (x=input.length;x>0;x--){
        balik = balik + input[x-1];
    }
    return balik;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log(' ') 
console.log('no 6') 

function dataHandling2(input) {
    input.splice(1, 1, "Roman Alamsyah Elsharawy")     
    input.splice(2, 1, "Provinsi Bandar Lampung") 
    input.splice(4, 1, "Pria") 
    input.splice(5, 0, "SMA Internasional Metro") 
    console.log(input) 

    var tgl = input[3];
    var splitTgl = tgl.split("/");

    var bulan="";
    switch(parseInt(splitTgl[1])) {
        case 1:  { bulan = "Januari"; break; }
        case 2:  { bulan = "Februari"; break; }
        case 3:  { bulan = "Maret"; break; }
        case 4:  { bulan = "April"; break; }
        case 5:  { bulan = "Mei"; break; }
        case 6:  { bulan = "Juni"; break; }
        case 7:  { bulan = "Juli"; break; }
        case 8:  { bulan = "Agustus"; break; }
        case 9:  { bulan = "September"; break; }
        case 10:  { bulan = "Oktober"; break; }
        case 11:  { bulan = "November"; break; }
        case 12:  { bulan = "Desember"; break; }
    }
    console.log(bulan) ;

    var sortdesc = splitTgl.sort(function (value1, value2) { return value2 - value1 } ) ;
    console.log(sortdesc);

    splitTgl = tgl.split("/");
    var slug = splitTgl.join("-");
    console.log(slug);

    var nama = input[1];
    console.log(nama.slice(0,15));

}

 var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
 dataHandling2(input);
