//no 1
console.log('no 1');
function arrayToObject(arr) {
    var objX = {}
    if (arr.length == 0){
        console.log(objX)
    } else {
        var now = new Date()
        var thisYear = now.getFullYear() 
        for (i=0; i < arr.length; i++){
            var data = arr[i]
            
            var umur = thisYear - data[3]
            if (typeof data[3] === 'undefined'  || thisYear < data[3]){
                umur = 'Invalid birth year'
            }
            var obj = {
                firstName : data[0],
                lastName: data[1],
                gender: data[2],
                age: umur
            }
            var nmr = i+1
            objX[nmr+'. ' + obj.firstName + ' ' + obj.lastName]  = obj
            
            console.log(objX)
        
        }
    }
    
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
{
  '1. Bruce Banner': { 
    firstName: 'Bruce', 
    lastName: 'Banner', 
    gender: 'male', 
    age: 45 
  },
  '2. Natasha Romanoff': {
    firstName: 'Natasha',
    lastName: 'Romanoff',
    gender: 'female',
    age: 'Invalid Birth Year'
  }
} 
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/* 
{
  '1. Tony Stark': {
    firstName: 'Tony', 
    lastName: 'Stark', 
    gender: 'male', 
    age: 40 
  },
  '2. Pepper Pots': {
    firstName: 'Pepper',
    lastName: 'Pots',
    gender: 'female',
    age: 'Invalid Birth Year'
  }
} 
*/
 
// Error case 
arrayToObject([]) // "{}"


//no 2
console.log(' ');
console.log('no 2');
function shoppingTime(memberId='', money=0) {
    if (memberId == ''){
        console.log('Mohon maaf, toko X hanya berlaku untuk member saja')
    } else {
        var produk = []
        var duitAwal = money
        var kembalian = 0

        while(money > 0) { 
            if (money >= 1500000 && produk.indexOf('Sepatu Stacattu')==-1) {
                produk.push('Sepatu Stacattu')
                money -= 1500000
            } else if (money >= 500000 && produk.indexOf('Baju Zoro')==-1){
                produk.push('Baju Zoro')
                money -= 500000
            } else if (money >= 250000 && produk.indexOf('Baju H&N')==-1){
                produk.push('Baju H&N')
                money -= 250000
            } else if (money >= 175000 && produk.indexOf('Sweater Uniklooh')==-1){
                produk.push('Sweater Uniklooh')
                money -= 175000
            } else if (money >= 50000 && produk.indexOf('Casing Handphone')==-1){
                produk.push('Casing Handphone')
                money -= 50000
            } else {
                kembalian = money
                money -= money
                if (produk.length==0){
                    console.log('Mohon maaf, uang tidak cukup')
                }
            }
        }


        if (produk.length>0){
            var obj = {
                memberId: memberId,
                money: duitAwal,
                listPurchased: produk,
                changeMoney: kembalian
            }
            return obj;
        }
    }
    
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000))
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000))
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000))//Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000))//Mohon maaf, uang tidak cukup
console.log(shoppingTime()) ////Mohon maaf, toko X hanya berlaku untuk member saja



console.log(' ');
console.log('no 3');
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    var penumpang = ''
    var dari = ''
    var ke = ''
    var hasil = []
    for(i=0;i<arrPenumpang.length;i++){
        var tarif = 0
        var data = arrPenumpang[i]
        penumpang = data[0]
        dari = data[1]
        ke = data[2]

        for(j=rute.indexOf(dari);j<rute.indexOf(ke);j++){
            tarif += 2000
        }

        var obj = {
            penumpang: penumpang, 
            naikDari: dari, 
            tujuan: ke, 
            bayar: tarif
        }

        hasil.push(obj)
    }
    
    console.log(hasil)
  }
   
//TEST CASE
naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']])
//console.log();
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
naikAngkot([])
//console.log(); //[]


