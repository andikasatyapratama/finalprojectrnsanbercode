console.log('no 1') 
class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
}
 
const sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


console.log(' ')
class Ape  extends Animal {
    constructor(name, legs = 2, cold_blooded = false) {
        super(name);
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
    yell() {
        console.log('Auooo');;
    }
}

class Frog  extends Animal {
    constructor(name, legs = 4, cold_blooded = true) {
        super(name);
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
    jump() {
        console.log('hop hop');
    }
}

const sungokong = new Ape("kera sakti");
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell(); // "Auooo"

console.log(' ')

const kodok = new Frog("buduk");
console.log(kodok.name); // "buduk"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // true
kodok.jump(); // "hop hop"



console.log(' ')
console.log('no 2')

class Clock {
    constructor({ template }) {
        this._template = template;
    }
  
    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this._template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }
    
    stop() {
        clearInterval(this._timer);
    }
    
    start() {
      this.render();
      this._timer = setInterval(() => this.render(), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();

