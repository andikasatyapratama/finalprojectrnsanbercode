//if - else 

// Output untuk Input nama = '' dan peran = ''
var nama = ""
var peran = ""
if (nama=="" && peran==""){
    console.log("Nama harus diisi!"); 
}

//Output untuk Input nama = 'John' dan peran = ''
var nama = "John"
var peran = ""
if (nama=="John" && peran==""){
    console.log("Halo John, Pilih peranmu untuk memulai game!"); 
}
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
var nama = "Jane"
var peran = "Penyihir"
if (nama=="Jane" && peran=="Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, Jane"); 
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"); 
}
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
var nama = "Jenita"
var peran = "Guard"
if (nama=="Jenita" && peran=="Guard"){
    console.log("Selamat datang di Dunia Werewolf, Jenita"); 
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."); 
}

//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
var nama = "Junaedi"
var peran = "Werewolf"
if (nama=="Junaedi" && peran=="Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, Junaedi"); 
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" ); 
}

//switch case

var tanggal = 17; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 8; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2021; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

var bln = "";

var strTgl  = String(tanggal);
var strThn  = String(tahun);

switch(bulan) {
    case 1:   { bln = "Januari"; break; }
    case 2:   { bln = "Februari"; break; }
    case 3:   { bln = "Maret"; break; }
    case 4:   { bln = "April"; break; }
    case 5:   { bln = "Mei"; break; }
    case 6:   { bln = "Juni"; break; }
    case 7:   { bln = "Juli"; break; }
    case 8:   { bln = "Agustus"; break; }
    case 9:   { bln = "September"; break; }
    case 10:   { bln = "Oktober"; break; }
    case 11:   { bln = "November"; break; }
    case 12:   { bln = "Desember"; break; }
    default:  { bln = ""; break; }
}

console.log(strTgl.concat(" ").concat(bln).concat(" ").concat(strThn));

