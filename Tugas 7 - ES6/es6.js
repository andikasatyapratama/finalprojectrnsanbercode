//no 1
console.log('no 1');
const golden = () =>  console.log("this is golden!!")
golden()


//no 2
console.log(' ');
console.log('no 2');
const newFunction = function literal(firstName, lastName){

    let fullName = () => console.log(firstName + " " + lastName)
    let person = {firstName, lastName, fullName}

    return person
}
   
//Driver Code
const person = newFunction("William", "Imoh");
console.log(person);
person.fullName(); 


//no 3
console.log(' ');
console.log('no 3');

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)


//no 4
console.log(' ');
console.log('no 4');
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


//no 5
console.log(' ');
console.log('no 5');
const planet = "earth"
const view = "glass"

const before =  `Lorem ${view} dolor sit amet, ` +
                `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
                `incididunt ut labore et dolore magna aliqua. Ut enim` +
                ` ad minim veniam`

// Driver Code
console.log(before) 