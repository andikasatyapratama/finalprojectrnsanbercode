function readBooksPromise(time, book){
    console.log(`saya mulai membaca ${book.name}`);
    return new Promise(function (resolve, reject){
        setTimeout(() => {
            let sisawaktu = time - book.timeSpent;
            if (sisawaktu >= 0){
                console.log(
                    `saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisawaktu}`
                );
                resolve(sisawaktu);
            } else {
                console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`);
                reject(sisawaktu);
            }
        }, book.timeSpent)
    });
}
module.exports = readBooksPromise;
