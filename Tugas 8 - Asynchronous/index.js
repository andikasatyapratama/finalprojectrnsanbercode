
// di index.js
const readBooks = require('./callback.js')
 
const books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
var i = 0;
var waktu = 10000;
readBooks(waktu , books[i], function(sisawaktu) {
    i++;
    readBooks(sisawaktu , books[i], function(sisawaktu) {
        i++;
        readBooks(sisawaktu , books[i], function(sisawaktu) {
            books[0].name='';
            readBooks(sisawaktu , books[0], function(sisawaktu) {});                    
        });            
    });    
});
