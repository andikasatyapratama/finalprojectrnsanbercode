import React, {useEffect} from 'react'
import { View, Text, Image, StyleSheet, SafeAreaView, TextInput, Pressable   } from 'react-native'

export default function LoginScreen() {
    const uname = 'andika';
    const passw = 'bbb';

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Image
                style={styles.logoDrawwer}
                    source={require('./assets/Logo.png')}
                />
            </View>
            
            <View style={{flexDirection:'column'}}>
                <View style={styles.loginCaptionArea}>
                    <Text style={{fontSize:28,color:'#003366'}}>Login</Text>
                </View>
                <View style={styles.formUsername}>
                        <SafeAreaView>
                    <Text>Username / Email</Text>
                            <TextInput style={styles.input} value={uname} />
                        </SafeAreaView>
                </View>
                <View style={styles.formPassword}>
                    
                    <SafeAreaView>
                    <Text>Password</Text>
                        <TextInput style={styles.input} value={passw} secureTextEntry={true} />
                    </SafeAreaView>
                </View>
                
                <View style={styles.tombolLoginArea}>
                    <Pressable style={styles.button} >
                        <Text style={styles.buttonText}>Masuk</Text>
                    </Pressable>
                    <Text style={{color:'#3EC6FF'}}>atau</Text>                    
                    <Pressable style={styles.button2} >
                        <Text style={styles.buttonText2}>Daftar ?</Text>
                    </Pressable>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        padding:20,
        alignItems:'center'
    },
    header:{
        padding:20
    },
    logoDrawwer:{
        marginTop:50,
        marginLeft:30,
        alignItems:'center'
    },
    loginCaptionArea:{
        alignItems:'center'
    },
    formUsername:{
        paddingTop:40,
        alignItems:'center'
    },
    formPassword:{
        paddingTop:15,
        alignItems:'center'
    },
    tombolLoginArea:{
        paddingTop:30,
        alignItems:'center'
    },
    input: {
        height: 40,
        width:200,
        margin: 0,
        borderWidth: 1,
        padding: 10,
        borderColor:'#003366'
    },
    button: {
        marginBottom:10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 8,
        paddingHorizontal: 20,
        borderRadius: 10,
        backgroundColor: 'white',
        borderColor:'#3EC6FF',
        borderWidth:1
    },
    buttonText: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: '#3EC6FF',
    },
    button2: {
        marginTop:10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 8,
        paddingHorizontal: 20,
        borderRadius: 10,
        backgroundColor: '#003366',
        borderColor:'#3EC6FF',
        borderWidth:1
    },
    buttonText2: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: '#fff',
    }
});