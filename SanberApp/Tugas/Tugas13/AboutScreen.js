import React, {useEffect} from 'react'
import { View, Text, Image, StyleSheet   } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

export default function AboutScreen() {

    return (
        <View style={styles.container}>
            <View style={styles.titleCaptionArea}>
                <Text style={{fontSize:28,color:'#003366'}}>Tentang Saya</Text>
            </View>
            <View style={styles.fotoProfil}>
                <Image
                style={styles.backGrPhoto}
                    source={require('./assets/BackGPhoto.png')}
                />
                <Image
                style={styles.fotoDrawwer}
                    source={require('./assets/UserIcon.png')}
                />
            </View>
            
            <View style={{flexDirection:'column',paddingBottom:10}}>
                <View style={styles.namauser}>
                    <Text style={{fontSize:20}}>Andika Satyapratama</Text>
                </View>
                <View style={styles.pekerjaan}>
                <Text style={{fontSize:15}}>React Native Developer</Text>
                </View>
                
            </View>

            <View style={{flexDirection:'column',backgroundColor:'#EFEFEF',margin:10,padding:15}}>
                <View style={{borderBottomColor: 'black', borderBottomWidth: 1,}}>
                    <View>
                        <Text style={{fontSize:20}}>Portofolio </Text>
                    </View>
                </View>
                <View style={{flexDirection:'row',width:230}}>
                    <View style={{alignItems:'center',padding:8}}>
                        <Icon
                            size={30}
                            name="gitlab"
                            >
                        </Icon>
                        <Text style={{fontSize:15}}>@andikagitlab </Text>
                    </View>
                    <View style={{alignItems:'center',padding:8}}>
                        <Icon
                            size={30}
                            name="github"
                            >
                        </Icon>
                        <Text style={{fontSize:15}}>@andikagithub </Text>
                    </View>
                </View>
            </View>

            <View style={{flexDirection:'column',backgroundColor:'#EFEFEF',margin:10,padding:15}}>
                <View style={{borderBottomColor: 'black', borderBottomWidth: 1,}}>
                    <View>
                        <Text style={{fontSize:20}}>Contact </Text>
                    </View>
                </View>
                <View style={{flexDirection:'row',width:230}}>
                    <View style={{alignItems:'center',padding:9}}>
                            <Text>Linked <Icon
                                size={20}
                                name="linkedin-square"
                                color='#0077B5'
                                >
                            </Icon></Text>  
                            
                        <Text style={{fontSize:8}}>@andikasatya </Text>
                    </View>
                    <View style={{alignItems:'center',padding:9}}>
                        <Image
                        style={{width:22,height:22}}
                            source={require('./assets/wa.png')}
                        />
                        <Text style={{fontSize:8}}>0812345678 </Text>
                    </View>
                    <View style={{alignItems:'center',padding:9}}>
                        <Image
                            style={{width:22,height:22}}
                                source={require('./assets/gmail.png')}
                            />
                        <Text style={{fontSize:8}}>andika@gmail.com </Text>
                    </View>
                </View>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        padding:20,
        alignItems:'center'
    },
    titleCaptionArea:{
        paddingTop:50,
        paddingBottom:20,
        alignItems:'center'
    },
    backGrPhoto:{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    fotoProfil:{
        padding:35,
    },
    fotoDrawwer:{
        alignItems:'center'
    },
    namauser:{
        paddingTop:20,
        alignItems:'center'
    },
    pekerjaan:{
        paddingTop:1,
        alignItems:'center'
    }
});