import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button, SafeAreaView, FlatList, TouchableOpacity } from 'react-native'
import { Data }from './data'
export default function Home({route, navigation}) {
    const { username } = route.params;
    const [totalPrice, setTotalPrice] = useState(0);
    
    const currencyFormat=(num)=> {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    
    const updateHarga =(price)=>{
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
        
        //? #Bonus (10 poin) -- HomeScreen.js --
        //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->
            
    }

    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
            {/* //? #Soal No 2 (15 poin) -- Home.js -- Function Home
            //? Buatlah 1 komponen FlatList dengan input berasal dari data.js   
            //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal) -->

            //? #Soal No 3 (15 poin) -- HomeScreen.js --
             //? Buatlah styling komponen Flatlist, agar dapat tampil dengan baik di device untuk layouting bebas  --> */
             
             }

            <View style={styles.content}>
                
                <View style={{padding:10}}>
                    <Text>Silahkan ketuk pada item untuk membeli barang</Text>
                </View>
                <SafeAreaView>
                    <FlatList
                        data={Data}
                        numColumns={2}
                        keyExtractor={(item)=>item.id}                        
                        renderItem={({item})=>{
                            let harga = item.harga
                            return(
                                <>
                                    <TouchableOpacity style={styles.itemContainer} onPress={() => updateHarga(harga)} >
                                        <View style={{flexDirection:'column', borderRadius:8, borderWidth:1, padding:5}}>
                                            <Image
                                                style={{height:100, width:100, margin:20, alignSelf:'center'}}
                                                source={item.image} 
                                            />
                                            <View style={styles.nameItem}>
                                                <Text style={{fontWeight: "bold"}}>{item.title}</Text>
                                                <Text style={{color:'blue',fontStyle: 'italic'}}>({item.type})</Text>
                                                <Text>{item.desc}</Text>
                                                <Text style={{color:'green'}}>Harga : {harga}</Text>
                                            </View>                                    
                                        </View>
                                    </TouchableOpacity>                                
                                    <View style={{borderBottomWidth:1, borderBottomColor:'#a8aaab'}}></View>
                                
                                </>
                            )
                        }}
                    />

                </SafeAreaView>
            </View>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{        
        margin: 0,
        alignItems:'center',
        borderColor:'grey',    
    },
    
    itemContainer:{ 
        margin: 2,
        width:170,
        backgroundColor:'white',
        flexDirection:'row',
        paddingTop:0,
        paddingHorizontal:0,
        paddingVertical:0,
        justifyContent:'space-between'
    },
    nameItem:{
        paddingLeft:0,
        
    }

})
