const Data = [
    {
        id : "1",
        image: require("./asset/profile1.png"),
        name:'Achmad Hilmy' ,
        message:'Hello Achmad Hilmy',
        time:'12.00',
        totalMessage:'2'
    },
    {
        id : "2",
        image: require("./asset/profile2.png"),
        name:'Profile 2' ,
        message:'Hello profile 2',
        time:'12.02',
        totalMessage:'4'
    },
    {
        id : "3",
        image: require("./asset/profile3.png"),
        name:'Profile 3' ,
        message:'Hello profile 3',
        time:'12.05',
        totalMessage:'1'
    }
]

export { Data }