import React, {useState, useEffect} from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import firebase from 'firebase';
import 'firebase/auth'

export default function Register({navigation}) {
    const firebaseConfig={
        apiKey: "AIzaSyC1PimfNkGhT3hV52poqTgyn8TYEaSBGIA",
        authDomain: "andikasatyasanbercode.firebaseapp.com",
        projectId: "andikasatyasanbercode",
        storageBucket: "andikasatyasanbercode.appspot.com",
        messagingSenderId: "601330928545",
        appId: "1:601330928545:web:dfede4f21ec171c479d9e0"
     };
     if(!firebase.apps.length){
         firebase.initializeApp(firebaseConfig)
     }
      const [email, setEmail] = useState("");
      const [password, setPassword] = useState("");

      const submit=()=>{
          const data = {
              email,
              password
          }
          console.log(data)
          firebase.auth().createUserWithEmailAndPassword(email, password).then(()=>{
            console.log('Register Berhasil');
              navigation.navigate("Home");
          }).catch(()=>{
            console.log("register gagal")
          })
      }
    return (
        <View style={styles.container}>
            <Text>Register</Text>
            <TextInput 
                style={styles.input}
                placeholder="Masukkan Email"
                value={email}
                onChangeText={(value)=>setEmail(value)}

            />
             <TextInput 
                secureTextEntry={true}
                style={styles.input}
                placeholder="Password (minimal 6 karakter)"
                value={password}
                onChangeText={(value)=>setPassword(value)}
            />
            <Button onPress={submit} title="REGISTER"/>
            <TouchableOpacity style={{marginTop: 30}}
            onPress={()=>navigation.navigate("Login")}
            //onPress={()=>alert("hello world")}
            >
                <Text>Login</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    input:{
        borderWidth:1,
        borderColor:'grey',
        paddingHorizontal:10,
        paddingVertical: 10,
        width: 300,
        marginBottom: 10,
        borderRadius: 6,
        marginTop: 10
    }
})
