import React from 'react'
import { StyleSheet, Image, Text, View } from 'react-native'

export default function AboutScreen({navigation}) {
    return (
        <View style={styles.container}>
            <Image
                style={{width:100, height:120, margin:20, alignSelf:'center'}}
                source={require("../assets/fotoprofil.png")}
            />
            <Text style={{fontSize:30}}>Andika Satyapratama</Text>
            <Text>React Native Developer</Text>
            <View style={{marginVertical: 15}}/>
            
            
            <View style={{borderWidth:1,borderRadius:8,padding:20, width:330}}>
                <Text style={{fontSize:20,fontWeight:'bold'}}>Portofolio</Text>
            
                <Text>
                    <Image
                    style={{width:20, height:20}}
                    source={require("../assets/gitlab.png")}
                /> https://gitlab.com/andikasatyapratama/
                </Text>
            </View>

            <View style={{marginVertical: 10}} />

            <View style={{borderWidth:1,borderRadius:8,padding:20, width:330}}>
                <Text style={{fontSize:20,fontWeight:'bold'}}>Kontak</Text>
                <Text>
                        <Image
                        style={{width:20, height:20}}
                        source={require("../assets/mail.png")}
                    /> andy.pratama91@gmail.com
                </Text>
                <Text>
                        <Image
                        style={{width:20, height:20}}
                        source={require("../assets/wa.png")}
                    /> 081333314791
                </Text>
                <Text>
                        <Image
                        style={{width:20, height:20}}
                        source={require("../assets/ig.png")}
                    /> @andikasatyapratama
                </Text>
            </View>
            <View style={{marginVertical: 30}}/>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },

    buttonL: {
        width: 230,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'white',
      },
      buttonR: {
        width: 230,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'blue',
      },

      textL: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'black',
      },

      textR: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
      },
      buttonA: {
        width: 120,
        height:60,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'black',
      },

      textA: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
      }
})
