import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, Image, Button, SafeAreaView, FlatList, TouchableOpacity } from 'react-native'
//import { Data }from './data'
import axios from 'axios'

export default function Detail({route, navigation}) {
    const { itemID } = route.params;
    const [Data, setItems] = useState("");
    const GetData=()=>{
        axios.get('http://114.4.37.148/tes/sanbercode-andika.html')
        .then(res=>{
            const data1 = (res.data.data)
            console.log('res: ',data1)
            setItems(data1)
        }).catch(err=>{
            console.log('error: ',err)
        })
    }

    useEffect(() => {
        GetData()
     }, [])

    return (
        <View style={styles.container}>

            <SafeAreaView>
                <FlatList
                    data={Data}
                    keyExtractor={(item)=>item.id}                        
                    renderItem={({item})=>{
                        let harga = item.harga
                        console.log("asdfasdf "+itemID + item.id)
                        if (itemID == item.id) {
                            return(
                                <>
                                        <View style={{flexDirection:'column', borderRadius:8, padding:5}}>
                                            <Image
                                                style={{height:500, width:300, margin:20, alignSelf:'center'}}
                                                source={ {uri: item.image} }
                                            />
                                            <View>
                                                <Text style={{fontWeight: "bold"}}>{item.title}</Text>
                                                <Text style={{color:'blue',fontStyle: 'italic'}}>({item.type})</Text>
                                                <Text>{item.desc}</Text>
                                                <Text style={{color:'green'}}>Harga : {harga}</Text>
                                            </View>                                    
                                        </View>                            
                                
                                </>
                            )
                            }
                    }}
                />

            </SafeAreaView>
        </View>

        
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center'
    }
})
