import React from 'react'
import { Image, StyleSheet, Text, View, Pressable } from 'react-native'

export default function GetStarted({navigation}) {
    return (
        <View style={styles.container}>
            <Image
                style={{width:300, height:90, margin:40, alignSelf:'center'}}
                source={require("../assets/title_toko_buku.png")}
            />
            <View style={{marginVertical: 5}}/>
            
            <Pressable style={styles.buttonL} onPress={()=>navigation.navigate("Login")}>
                <Text style={styles.textL}>Sign In / Log In</Text>
            </Pressable>

            <View style={{marginVertical: 10}}/>

            <Pressable style={styles.buttonR} onPress={()=>navigation.navigate("Register")}>
                <Text style={styles.textR}>Sign Up / Register</Text>
            </Pressable>
            
            <View style={{marginVertical: 50}}/>
            <Pressable style={styles.buttonA} onPress={()=>navigation.navigate("About")}>
                <Text style={styles.textA}>About</Text>
            </Pressable>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },

    buttonL: {
        width: 230,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'white',
      },
      buttonR: {
        width: 230,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'blue',
      },

      textL: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'black',
      },

      textR: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
      },
      buttonA: {
        width: 120,
        height:60,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'black',
      },

      textA: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
      }
})
