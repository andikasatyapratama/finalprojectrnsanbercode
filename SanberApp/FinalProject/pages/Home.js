import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, Image, Button, SafeAreaView, FlatList, TouchableOpacity } from 'react-native'
//import * as firebase from 'firebase';
import firebase from 'firebase/app';
import 'firebase/auth'
import axios from 'axios'

//import { Data }from './data'


export default function Home({navigation}) {
    const [user, setUser] = useState({})
    const [Data, setItems] = useState("");
   
    const GetData=()=>{
        axios.get('http://114.4.37.148/tes/sanbercode-andika.html')
        .then(res=>{
            const data1 = (res.data.data)
            console.log('res: ',data1)
            setItems(data1)
        }).catch(err=>{
            console.log('error: ',err)
        })
    }

    useEffect(() => {
       const userInfo =firebase.auth().currentUser
       setUser(userInfo)
       GetData()
    }, [])
    const onLogout=()=>{
        firebase.auth()
        .signOut()
            .then(()=>{
                console.log('user Sign out');
                navigation.navigate('GetStarted')
            }
            )
        
    }
    

    const [totalPrice, setTotalPrice] = useState(0);
    
    const currencyFormat=(num)=> {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    
    const updateHarga =(price)=>{
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
        
    }

    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{user.email}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>

            <View style={styles.content}>
                
                    <FlatList
                        style={{height:500,borderTopWidth:2,borderBottomWidth:2}}
                        data={Data}
                        numColumns={2}
                        keyExtractor={(item)=>item.id}                        
                        renderItem={({item})=>{
                            var harga = item.harga //{item.image}  product1.jpg
                            console.log(item.image)
                            return(
                                <>
                                    <TouchableOpacity style={styles.itemContainer} onPress={
                                        ()=>navigation.navigate("Detail",{
                                            itemID : item.id
                                        })} >
                                        <View style={{flexDirection:'column', borderRadius:8, borderWidth:1, padding:5}}>
                                            <Image
                                                style={{height:140, width:100, margin:20, alignSelf:'center'}}
                                                source={ {uri: item.image} }
                                            />
                                            <View>
                                                <Text style={{fontWeight: "bold"}}>{item.title}</Text>
                                                <Text style={{color:'blue',fontStyle: 'italic'}}>({item.type})</Text>
                                                <Text style={{minHeight:40}}>{item.desc}</Text>
                                                <Text style={{color:'green'}}>Harga : {harga}</Text>
                                                <Button onPress={() => updateHarga(harga)} title="Add"/>
                                            </View>                                    
                                        </View>
                                    </TouchableOpacity>                                
                                
                                </>
                            )
                        }}
                    />

            </View>
            <View style={{alignContent:'flex-end',margin:5}}>
                <Button onPress={() => alert("Total belanjaan anda "+currencyFormat(totalPrice))} title="Checkout"/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    //container:{
    //    flex:1,
    //    alignItems:'center',
    //    justifyContent:'center'
    //}

    container:{
        flex: 1,        
        backgroundColor:'white'
    },  
    content:{        
        margin: 0,
        alignItems:'center',
        borderColor:'grey',    
    },
    
    itemContainer:{ 
        margin: 2,
        width:170,
        backgroundColor:'white',
        flexDirection:'row',
        paddingTop:0,
        paddingHorizontal:0,
        paddingVertical:0,
        justifyContent:'space-between'
    }
})
