import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";

const Quiz = () => {
  const [name, setName] = useState('john') 

  useEffect(() => {
    setTimeout(() => {
      setName('zaky')
    }, 2000)
    return() => {
      setName('achm')
    }
  },[])

  return (
    <View>
      <Text>{name}</Text>
    </View>
  )
}
export default Quiz


